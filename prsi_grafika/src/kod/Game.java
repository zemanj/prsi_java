package kod;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;



import javax.swing.*;



import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


public class Game extends JFrame {
    Deck deck;
    List<Player> players;
    Card topCard;
    int currentPlayerIndex;
    int counterOf7 = 1;
    Scanner scanner = new Scanner(System.in);
    private int selectedCardIndex = -2;
    class Card {
        String rank;
        String suit;
    
        Card(String rank, String suit) {
            this.rank = rank;
            this.suit = suit;
            //System.out.println("Card created: " + rank + " of " + suit);
        }
    }

    class Deck {
        List<Card> cards;

        Deck() {
            cards = new ArrayList<Card>();
            String[] ranks = {"7", "8", "9", "10", "S", "M", "K", "E"};
            String[] suits = {"srdce", "zaludy", "zeleny", "kule"};
            for (String suit : suits) {
                for (String rank : ranks) {
                    cards.add(new Card(rank, suit));
                }
            }
            Collections.shuffle(cards);
            
        }

        Card deal() {
            if (cards.isEmpty()) {
                throw new IllegalStateException("Deck is empty");
            }
            return cards.remove(cards.size() - 1);
        }
    }

    class Player {
        List<Card> hand;
        String name;
    
        Player(String name) {
            this.name = name;
            hand = new ArrayList<Card>();
        }
    
        void takeCard(Card card) {
            hand.add(card);
        }
    
        Card playCard(int index) {
            return hand.remove(index);
        }
    }

    public Game() {
        
        
        deck = new Deck();
        players = new ArrayList<Player>();
        players.add(new Player("Player 1"));
        players.add(new Player("Player 2"));
        currentPlayerIndex = 0;
    
        
    
        
    
        
    }

    public void paint(Graphics g) {
        super.paint(g);
        // Zde můžete přidat kód pro vykreslení prvků hry, například karet nebo hráčů
    }

    void start() {
        for (Player player : players) {
            for (int i = 0; i < 4; i++) {
                player.takeCard(deck.deal());
            }
        }
        topCard = deck.deal();
        System.out.println("top card: " + topCard.rank + " of " + topCard.suit);
    }

    
    void playTurn() {
        
        System.out.println("---------------------------------------------------------------------------------------------------");
        // ...
        setSize(1920, 1080);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel panel = new JPanel() {
            
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);

                ImageIcon backgroundIcon = new ImageIcon("src/obrazky/pozadi.jpg");
                Image backgroundImage = backgroundIcon.getImage();
                g.drawImage(backgroundImage, 0, 0, getWidth(), getHeight(), this);

                ImageIcon topcardIcon = new ImageIcon("src/obrazky/"+topCard.suit+topCard.rank+".png");
                Image topcard = topcardIcon.getImage();
                g.drawImage(topcard, 910, 300, 100, 150, this);

                Player currentPlayer = players.get(currentPlayerIndex);

                ImageIcon playerIcon = new ImageIcon("src/obrazky/p"+currentPlayerIndex+".png");
                Image player = playerIcon.getImage();
                g.drawImage(player, 50, 50, 100, 100, this);

                ImageIcon plusCardIcon = new ImageIcon("src/obrazky/pluscard.png");
                Image plusCard = plusCardIcon.getImage();
                g.drawImage(plusCard, 200 - (- 1 - 6) * 150, 800, 100, 150, this);

                for (int i = 0; i < currentPlayer.hand.size(); i++) {
                    ImageIcon imageIcon = new ImageIcon("src/obrazky/" + currentPlayer.hand.get(i).suit + currentPlayer.hand.get(i).rank + ".png");
                    Image karta = imageIcon.getImage();
                    g.drawImage(karta, 200 - (i - 6) * 150, 800, 100, 150, this);
                }
            }
        };

        for (Player player : players) {
            System.out.println("Player Name: " + player.name);
            System.out.println("Cards in Hand: ");
            for (int i = 0; i < player.hand.size(); i++) {
                System.out.println((i+1) + ": " + player.hand.get(i).rank + " of " + player.hand.get(i).suit);
                
            }
            System.out.println("0: l�znout si");
            System.out.println();
        }

        selectedCardIndex = -2;
        
        panel.addMouseListener(new MouseAdapter() {
            
            @Override
            public void mouseClicked(MouseEvent e) {
                Player currentPlayer = players.get(currentPlayerIndex);
                for (int i = 0; i < currentPlayer.hand.size(); i++) {
                    // Check if the click was within the bounds of the card
                    if (e.getX() >= 200 - (i - 6) * 150 && e.getX() <= 200 - (i - 6) * 150 + 100 &&
                        e.getY() >= 800 && e.getY() <= 800 + 150) {
                        // If it was, store the index of the card
                        selectedCardIndex = i; 
                        System.out.println(selectedCardIndex);
                        break;
                    } else if (e.getX() >= 200 - (-1 - 6) * 150 && e.getX() <= 200 - (-1 - 6) * 150 + 100 &&
                               e.getY() >= 800 && e.getY() <= 800 + 150) {
                        // If it was, store the index of the card
                        selectedCardIndex = -1;
                        System.out.println(selectedCardIndex);
                        break;
                    }
                }
            }
        });
        
        System.out.println(selectedCardIndex+" cards");
        add(panel);
        setVisible(true);
        
        add(panel);
        setVisible(true);

        

        while(selectedCardIndex == -2) {
            // Wait for a valid card to be selected
            try {
                Thread.sleep(100); // Sleep for 100 milliseconds
            } catch (InterruptedException e) {
                e.printStackTrace();
                
            }
        }

        
/* 
        for (Player player : players) {
            System.out.println("Player Name: " + player.name);
            System.out.println("Cards in Hand: ");
            for (int i = 0; i < player.hand.size(); i++) {
                System.out.println((i+1) + ": " + player.hand.get(i).rank + " of " + player.hand.get(i).suit);
                
            }
            System.out.println("0: l�znout si");
            System.out.println();
        }
*/

System.out.println(selectedCardIndex+" pooooooo");
        Player currentPlayer = players.get(currentPlayerIndex);

        System.out.println(currentPlayer.name + ", choose a card to play (enter the number): ");
        
        int chosenCardIndex = selectedCardIndex;//scanner.nextInt() - 1;

        if (chosenCardIndex >= 0 && chosenCardIndex < currentPlayer.hand.size()) {
            Card card = currentPlayer.hand.get(chosenCardIndex);

            if (card.rank.equals(topCard.rank) || card.suit.equals(topCard.suit) || card.rank.equals("M")) {
                System.out.println("TopCard:" + topCard.rank + " of " + card.suit);

                System.out.println(currentPlayer.name + " plays " + card.rank + " of " + card.suit);
                
                topCard = currentPlayer.playCard(chosenCardIndex);
            
                switch (card.rank) {
                    case "E":
                        // The next player must play an "E"
                        Player nextPlayer = players.get((currentPlayerIndex + 1) % players.size());
                        boolean canCounter = false;
                        for (Card nextCard : nextPlayer.hand) {
                            if (nextCard.rank.equals("E")) {
                                canCounter = true;
                                break;
                            }
                        }
                        if (!canCounter) {
                            System.out.println(nextPlayer.name + " cannot play an 'E', skips turn");
                            currentPlayerIndex = (currentPlayerIndex + 2) % players.size();
                        } else {
                            currentPlayerIndex = (currentPlayerIndex + 1) % players.size();
                            card.suit = "x";
                        }
                        break;
                    case "7":
                        // The next player must play a "7"
                        nextPlayer = players.get((currentPlayerIndex + 1) % players.size());
                        canCounter = false;
                        System.out.println("7777777777777777777777777777777777777777777777777777777777777777777777");
                        for (Card nextCard : nextPlayer.hand) {
                            if (nextCard.rank.equals("7")) {
                                canCounter = true;
                                break;
                            }
                        }
                        
                        if (!canCounter) {
                            
                            System.out.println(nextPlayer.name + " cannot play a '7', skips turn");
                            
                            for (int j = 0; j < counterOf7; j++) {
                                nextPlayer.takeCard(deck.deal());
                                nextPlayer.takeCard(deck.deal());
                                System.out.println(nextPlayer.name+" bere "+ counterOf7*2 + "karet");
                            }
                            
                            currentPlayerIndex = (currentPlayerIndex + 2) % players.size();
                            
                            counterOf7 = 1;
                        } else {
                            currentPlayerIndex = (currentPlayerIndex + 1) % players.size();
                            card.suit = "x";
                            counterOf7++;
                            
                        }
                        break;
                    /*case "M":
                        // The next player must play the same suit
                        System.out.println(currentPlayer.name + ", choose a suit: ");
                        Scanner scanner = new Scanner(System.in);
                        String chosenSuit = scanner.nextLine();
                        topCard.suit = chosenSuit;
                        card.rank = "x";
                        currentPlayerIndex = (currentPlayerIndex + 1) % players.size();
                        break;*/
                    default:
                        // Move to the next player as usual
                        currentPlayerIndex = (currentPlayerIndex + 1) % players.size();
                        break;
                }

                    System.out.println("TopCard:" + topCard.rank + " of " + card.suit);
                    return;
                } else {
                    System.out.println("You cannot play this card. It must match the rank or suit of the top card.");
                }
            } else if (chosenCardIndex == -1) {
                System.out.println(currentPlayer.name + " takes a card.");
                currentPlayer.takeCard(deck.deal());
                currentPlayerIndex = (currentPlayerIndex + 1) % players.size();
            } else {
                System.out.println("Invalid card number. Please try again.");
            }
    }
        
    boolean isGameOver() {
        for (Player player : players) {
            if (player.hand.isEmpty()) {
                return true;
            }
        }
        return false;
    }
    
    

    Player getWinner() {
        for (Player player : players) {
            if (player.hand.isEmpty()) {
                return player;
            }
        }
        return null;
    }



    public static void main(String[] args) {
        Game game = new Game();
        game.start();
        while (!game.isGameOver()) {
            game.playTurn();
            game.repaint();
        }
        Player winner = game.getWinner();
        if (winner != null) {
            System.out.println("Winner is " + winner.name);
            
        }
    }
}