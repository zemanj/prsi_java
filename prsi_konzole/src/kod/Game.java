package kod;

import java.util.ArrayList;

import java.util.List;
import java.util.Scanner;



public class Game {
    Deck deck;
    List<Player> players;
    Card topCard;
    int currentPlayerIndex;
    int counterOf7 = 1;
    Scanner scanner = new Scanner(System.in);

    Game() {
        deck = new Deck();
        players = new ArrayList<>();
        players.add(new Player("Player 1"));
        players.add(new Player("Player 2"));
        currentPlayerIndex = 0;
    }

    void start() {
        for (Player player : players) {
            for (int i = 0; i < 4; i++) {
                player.takeCard(deck.deal());
            }
        }
        topCard = deck.deal();
        System.out.println("top card: " + topCard.rank + " of " + topCard.suit);
    }

    void playTurn() {
        System.out.println("---------------------------------------------------------------------------------------------------");

        for (Player player : players) {
            System.out.println("Player Name: " + player.name);
            System.out.println("Cards in Hand: ");
            for (int i = 0; i < player.hand.size(); i++) {
                System.out.println((i+1) + ": " + player.hand.get(i).rank + " of " + player.hand.get(i).suit);
            }
            System.out.println("0: líznout si");
            System.out.println();
        }

        Player currentPlayer = players.get(currentPlayerIndex);

        System.out.println(currentPlayer.name + ", choose a card to play (enter the number): ");
        
        int chosenCardIndex = scanner.nextInt() - 1;

        if (chosenCardIndex >= 0 && chosenCardIndex < currentPlayer.hand.size()) {
            Card card = currentPlayer.hand.get(chosenCardIndex);

            if (card.rank.equals(topCard.rank) || card.suit.equals(topCard.suit) || card.rank.equals("M")) {
                System.out.println("TopCard:" + topCard.rank + " of " + card.suit);

                System.out.println(currentPlayer.name + " plays " + card.rank + " of " + card.suit);
                
                topCard = currentPlayer.playCard(chosenCardIndex);
            
                switch (card.rank) {
                    case "E":
                        // The next player must play an "E"
                        Player nextPlayer = players.get((currentPlayerIndex + 1) % players.size());
                        boolean canCounter = false;
                        for (Card nextCard : nextPlayer.hand) {
                            if (nextCard.rank.equals("E")) {
                                canCounter = true;
                                break;
                            }
                        }
                        if (!canCounter) {
                            System.out.println(nextPlayer.name + " cannot play an 'E', skips turn");
                            currentPlayerIndex = (currentPlayerIndex + 2) % players.size();
                        } else {
                            currentPlayerIndex = (currentPlayerIndex + 1) % players.size();
                            card.suit = "x";
                        }
                        break;
                    case "7":
                        // The next player must play a "7"
                        nextPlayer = players.get((currentPlayerIndex + 1) % players.size());
                        canCounter = false;
                        System.out.println("7777777777777777777777777777777777777777777777777777777777777777777777");
                        for (Card nextCard : nextPlayer.hand) {
                            if (nextCard.rank.equals("7")) {
                                canCounter = true;
                                break;
                            }
                        }
                        
                        if (!canCounter) {
                            
                            System.out.println(nextPlayer.name + " cannot play a '7', skips turn");
                            
                            for (int j = 0; j < counterOf7; j++) {
                                nextPlayer.takeCard(deck.deal());
                                nextPlayer.takeCard(deck.deal());
                                System.out.println(nextPlayer.name+" bere "+ counterOf7*2 + "karet");
                            }
                            
                            currentPlayerIndex = (currentPlayerIndex + 2) % players.size();
                            
                            counterOf7 = 1;
                        } else {
                            currentPlayerIndex = (currentPlayerIndex + 1) % players.size();
                            card.suit = "x";
                            counterOf7++;
                            
                        }
                        break;
                    case "M":
                        // The next player must play the same suit
                        System.out.println(currentPlayer.name + ", choose a suit: ");
                        Scanner scanner = new Scanner(System.in);
                        String chosenSuit = scanner.nextLine();
                        topCard.suit = chosenSuit;
                        card.rank = "x";
                        currentPlayerIndex = (currentPlayerIndex + 1) % players.size();
                        break;
                    default:
                        // Move to the next player as usual
                        currentPlayerIndex = (currentPlayerIndex + 1) % players.size();
                        break;
                }

                    System.out.println("TopCard:" + topCard.rank + " of " + card.suit);
                    return;
                } else {
                    System.out.println("You cannot play this card. It must match the rank or suit of the top card.");
                }
            } else if (chosenCardIndex == -1) {
                System.out.println(currentPlayer.name + " takes a card.");
                currentPlayer.takeCard(deck.deal());
                currentPlayerIndex = (currentPlayerIndex + 1) % players.size();
            } else {
                System.out.println("Invalid card number. Please try again.");
            }
    }
    
    boolean isGameOver() {
        for (Player player : players) {
            if (player.hand.isEmpty()) {
                return true;
            }
        }
        return false;
    }

    Player getWinner() {
        for (Player player : players) {
            if (player.hand.isEmpty()) {
                return player;
            }
        }
        return null;
    }

    public static void main(String[] args) {
        Game game = new Game();
        game.start();
        while (!game.isGameOver()) {
            game.playTurn();
        }
        Player winner = game.getWinner();
        if (winner != null) {
            System.out.println("The winner is " + winner.name);
        }
    }
}