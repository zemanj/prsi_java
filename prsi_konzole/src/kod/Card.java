package kod;

public class Card {
    String rank;
    String suit;

    Card(String rank, String suit) {
        this.rank = rank;
        this.suit = suit;
        //System.out.println("Card created: " + rank + " of " + suit);
    }
}