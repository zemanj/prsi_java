package kod;

import java.util.ArrayList;
import java.util.List;

public class Player {
    List<Card> hand;
    String name;

    Player(String name) {
        this.name = name;
        hand = new ArrayList<>();
    }

    void takeCard(Card card) {
        hand.add(card);
    }

    Card playCard(int index) {
        return hand.remove(index);
    }
}