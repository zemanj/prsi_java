package kod;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Deck {
    List<Card> cards;

    Deck() {
        cards = new ArrayList<>();
        String[] ranks = {"7", "8", "9", "10", "S", "M", "K", "E"};
        String[] suits = {"srdce", "zaludy", "zeleny", "kule"};
        for (String suit : suits) {
            for (String rank : ranks) {
                cards.add(new Card(rank, suit));
            }
        }
        Collections.shuffle(cards);
        
    }

    Card deal() {
        if (cards.isEmpty()) {
            throw new IllegalStateException("Deck is empty");
        }
        return cards.remove(cards.size() - 1);
    }
}