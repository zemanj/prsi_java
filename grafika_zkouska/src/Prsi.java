import javax.swing.*;
import java.awt.*;

public class Prsi extends JFrame {

    public Prsi() {
        super("Obrazek s pozadim");
        setSize(1920, 1080);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


        String backgroundPath = "src/obrazky/pozadi.jpg";



        JPanel panel = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);

                ImageIcon backgroundIcon = new ImageIcon(backgroundPath);
                Image backgroundImage = backgroundIcon.getImage();
                g.drawImage(backgroundImage, 0, 0, getWidth(), getHeight(), this);


                for (int i = 7; i < 11; i++) {
                    ImageIcon imageIcon = new ImageIcon("src/obrazky/cerveny"+i+".png");
                    Image karta = imageIcon.getImage();
                    g.drawImage(karta, 910 - (i - 7) * 120, 800, 100, 150, this);
                }

                
            }
        };


        add(panel);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            Prsi game = new Prsi();
            game.setVisible(true);
        });
    }
}